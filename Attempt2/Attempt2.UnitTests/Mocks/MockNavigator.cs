﻿using DK.XamarinForms.Base.Services;
using DK.XamarinForms.Base.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attempt2.UnitTests.Mocks
{
	class MockNavigator : INavigator
	{

		private static Stack<IViewModel> NavStack = new Stack<IViewModel>();

		public IViewModel CurrentViewModel
		{
			get
			{
				if (NavStack.Count == 0) return null;
				return NavStack.Peek();
			}
		}

		public Task<IViewModel> PopAsync()
		{
			return Task.Run(() =>
			{
				if (!NavStack.Any()) return null;
				var vm = NavStack.Pop();
				vm.Popped();
				return vm;
			});
		}

		public Task<IViewModel> PopModalAsync()
		{
			return Task.Run(() =>
			{
				if (!NavStack.Any()) return null;
				var vm = NavStack.Pop();
				vm.Popped();
				return vm;
			});
		}

		public Task PopToRootAsync()
		{
			return Task.Run(() => NavStack.Clear());
		}

		public void RemoveViewModel<T>() where T : IViewModel
		{
			throw new NotImplementedException();
		}

		public Task<TViewModel> PushAsync<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : class, IViewModel
		{
			return Task.Run(() =>
			{
				TViewModel viewModel = Activator.CreateInstance<TViewModel>();
				if (setStateAction != null)
					viewModel.SetState(setStateAction);
				NavStack.Push(viewModel);
				return viewModel;
			});
		}

		public Task<TViewModel> PushAsync<TViewModel>(TViewModel viewModel) where TViewModel : class, IViewModel
		{
			return Task.Run(() =>
			{
				NavStack.Push(viewModel);
				return viewModel;
			});
		}

		public Task<TViewModel> PushModalAsync<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : class, IViewModel
		{
			return Task<TViewModel>.Run(() =>
			{
				TViewModel viewModel = Activator.CreateInstance<TViewModel>();
				if (setStateAction != null)
					viewModel.SetState(setStateAction);
				NavStack.Push(viewModel);
				return viewModel;
			});
		}

		public Task<TViewModel> PushModalAsync<TViewModel>(TViewModel viewModel) where TViewModel : class, IViewModel
		{
			return Task<TViewModel>.Run(() =>
			{
				NavStack.Push(viewModel);
				return viewModel;
			});
		}

	}
}
