﻿using Attempt2.Models;
using System.Threading.Tasks;

namespace Attempt2.Services
{
	public interface IJokeService
	{
		Task<Joke>GetNewJokeAsync();
	}
}
