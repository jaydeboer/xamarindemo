﻿using Attempt2.ViewModels;
using Attempt2.Views;
using Autofac;
using DK.XamarinForms.Base.Bootstrapping;
using DK.XamarinForms.Base.Factories;
using Jokester.Api.ClientAccess.Implementation;
using Xamarin.Forms;

namespace Attempt2
{
	public class Bootstrapper : AutofacBootstrapper
	{
		protected override void ConfigureApplication(IContainer container)
		{

			var viewFactory = container.Resolve<IViewFactory>();
			var mainPage = viewFactory.Resolve<MainPageVM>();
			var navigationPage = new Xamarin.Forms.NavigationPage(mainPage);

			App.MainPage = navigationPage;
		}

		protected override void RegisterViews(IViewFactory viewFactory)
		{
			viewFactory.Register<MainPageVM, MainPage>();
			viewFactory.Register<SecondPageVM, SecondPage>();
		}

		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

			var assembly = System.Reflection.Assembly.Load(new System.Reflection.AssemblyName("Attempt2"));
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".Services.Implmentation")).AsImplementedInterfaces().SingleInstance();
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".ViewModels")).AsSelf().InstancePerDependency();
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".Views")).AsSelf().InstancePerDependency();

			builder.RegisterType<JokeRepository>().AsImplementedInterfaces().SingleInstance();

		}

		public Bootstrapper(Application app)
		{
			App = app;
		}

		private readonly Application App;
	}
}
