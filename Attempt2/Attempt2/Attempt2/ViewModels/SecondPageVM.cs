﻿using DK.XamarinForms.Base.ViewModels;

namespace Attempt2.ViewModels
{
	public class SecondPageVM : ViewModelBase
	{
		public string Message { get; } = "We made it to the second page.";
	}
}
