﻿
using Xamarin.Forms;

namespace Attempt2
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			new Bootstrapper(this).Run();
		}
	}
}
