﻿using DK.XamarinForms.Base.ViewModels;

namespace Attempt3.ViewModels
{
	public class SecondPageVM : ViewModelBase
	{
		public string Message { get; set; } = "This is page 2.";
	}
}
