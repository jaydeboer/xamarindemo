﻿using Attempt3.Models;
using Attempt3.Services;
using DK.XamarinForms.Base.Services;
using DK.XamarinForms.Base.ViewModels;
using System.Windows.Input;
using Xamarin.Forms;

namespace Attempt3.ViewModels
{
	public class MainPageVM : ViewModelBase
	{
		public string Message { get; set; } = "Hello from shared lib";

		public ICommand GoToSecondViewCommand { get; private set; }
		public ICommand TellJokeCommand { get; private set; }

		public MainPageVM(INavigator navigator, IJokeService jokeService)
		{
			Navigator = navigator;
			JokeService = jokeService;

			GoToSecondViewCommand = new Command(
				async () => await Navigator.PushAsync<SecondPageVM>());
			TellJokeCommand = new Command(
				async() => JokeModel = await JokeService.GetNewJokeAsync());
		}

		private readonly INavigator Navigator;
		private readonly IJokeService JokeService;


		#region Joke Support

		public string FirstLine
		{
			get
			{
				return JokeModel?.FirstLine;
			}
		}

		public string Punchline
		{
			get
			{
				return JokeModel?.Punchline;
			}
		}

		private Joke _jokeModel = null;
		private Joke JokeModel
		{
			get { return _jokeModel; }
			set
			{
				if (SetProperty(ref _jokeModel, value))
				{
					OnPropertyChanged(nameof(FirstLine));
					OnPropertyChanged(nameof(Punchline));
				}
			}
		}

		#endregion

	}
}
