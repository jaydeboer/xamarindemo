﻿
using Xamarin.Forms;

namespace Attempt3
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			new Bootstrapper(this).Run();
		}
	}
}
