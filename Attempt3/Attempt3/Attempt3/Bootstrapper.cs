﻿using Attempt3.Services.Implementation;
using Attempt3.ViewModels;
using Attempt3.Views;
using Autofac;
using DK.XamarinForms.Base.Bootstrapping;
using DK.XamarinForms.Base.Controls;
using DK.XamarinForms.Base.Factories;
using Jokester.Api.ClientAccess.Implementation;

namespace Attempt3
{
	class Bootstrapper : AutofacBootstrapper
	{
		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

			var assembly = System.Reflection.Assembly.Load(new System.Reflection.AssemblyName("Attempt3"));
			
			builder.RegisterAssemblyTypes(assembly).
				Where(t => t.Namespace != null && t.Namespace.EndsWith(".ViewModels")).
				AsSelf().InstancePerDependency();
			builder.RegisterAssemblyTypes(assembly).
				Where(t => t.Namespace != null && t.Namespace.EndsWith(".Views")).
				AsSelf().InstancePerDependency();
			builder.RegisterType<JokesterJokeService>().AsImplementedInterfaces().SingleInstance();

			builder.RegisterType<JokeRepository>().AsImplementedInterfaces().SingleInstance();


		}
		protected override void ConfigureApplication(IContainer container)
		{
			var viewFactory = container.Resolve<IViewFactory>();
			var mainPage = viewFactory.Resolve<MainPageVM>();
			var navigationPage = new NavigationPage(mainPage);

			App.MainPage = navigationPage;
		}

		protected override void RegisterViews(IViewFactory viewFactory)
		{
			viewFactory.Register<MainPageVM, MainPageView>();
			viewFactory.Register<SecondPageVM, SecondPageView>();
		}

		public Bootstrapper(Xamarin.Forms.Application app)
		{
			App = app;
		}

		private readonly Xamarin.Forms.Application App;
	}
}
