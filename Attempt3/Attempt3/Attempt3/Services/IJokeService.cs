﻿using Attempt3.Models;
using System.Threading.Tasks;

namespace Attempt3.Services
{
	public interface IJokeService
	{
		Task<Joke> GetNewJokeAsync();
	}
}
