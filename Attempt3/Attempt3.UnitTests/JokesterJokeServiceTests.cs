﻿using Attempt3.Services.Implementation;
using Jokester.Api.ClientAccess;
using Jokester.Api.ClientAccess.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attempt3.UnitTests
{
	public class JokesterJokeServiceTests : AutomockingBase
	{

		[Test]
		public async Task WhenGetNewJokeIsSuccessful_TheReturnedJokeIsMappedToModel()
		{
			var expectedFirstLine = "Something";
			var expectedPunchline = "funny!";
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ReturnsAsync(
				new JokeApiModel()
				{
					Setup = expectedFirstLine,
					Punchline = expectedPunchline
				});

			var service = mock.Create<JokesterJokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(actual.FirstLine, Is.EqualTo(expectedFirstLine));
			Assert.That(actual.Punchline, Is.EqualTo(expectedPunchline));
		}

		[Test]
		public async Task WhenGetNewJokeReceivesNull_ADefaultJokeIsReturned()
		{
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ReturnsAsync(null);

			var service = mock.Create<JokesterJokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(string.IsNullOrWhiteSpace(actual.FirstLine), Is.False);
			Assert.That(string.IsNullOrWhiteSpace(actual.Punchline), Is.False);
		}

		[Test]
		public async Task WhenRepositoryThrowsException_ADefaultJokeIsReturned()
		{
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ThrowsAsync(new NullReferenceException());

			var service = mock.Create<JokesterJokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(string.IsNullOrWhiteSpace(actual.FirstLine), Is.False);
			Assert.That(string.IsNullOrWhiteSpace(actual.Punchline), Is.False);
		}

	}
}
