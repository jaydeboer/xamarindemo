﻿using EveryoneInThePool.Models;
using System.Threading.Tasks;

namespace EveryoneInThePool.Services
{
	public interface IJokeService
	{
		Task<Joke> GetNewJokeAsync(); 
	}
}
