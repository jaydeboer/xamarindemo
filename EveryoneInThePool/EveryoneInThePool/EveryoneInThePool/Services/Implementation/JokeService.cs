﻿using EveryoneInThePool.Models;
using Jokester.Api.ClientAccess;
using System.Threading.Tasks;

namespace EveryoneInThePool.Services.Implementation
{
	public class JokeService : IJokeService
	{

		public async Task<Joke> GetNewJokeAsync()
		{
			try
			{
				var result = await Repo.GetAsync();
				if (result == null)
					return Joke.Create(
						"Ever hear the one where the web service broke during the demo?",
						"No?  Me either");
				return Joke.Create(result.Setup, result.Punchline);
			}
			catch
			{
				return Joke.Create(
					"Ever hear the one where the web service broke during the demo?",
					"No?  Me either");
			}
			
		}

		public JokeService(IJokeRepository repo)
		{
			Repo = repo;
		}

		private readonly IJokeRepository Repo;
	}
}
