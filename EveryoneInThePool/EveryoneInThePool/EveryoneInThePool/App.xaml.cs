﻿
using Xamarin.Forms;

namespace EveryoneInThePool
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			new Bootstrapper(this).Run();
		}
	}
}
