﻿using DK.XamarinForms.Base.Services;
using DK.XamarinForms.Base.ViewModels;
using EveryoneInThePool.Models;
using EveryoneInThePool.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace EveryoneInThePool.ViewModels
{
	public class MainPageVM : ViewModelBase
	{
		public string Message { get; } = "Hello from shared library";

		public ICommand GoToSecondPageCommand { get; private set; }

		public MainPageVM() { }
		public MainPageVM(INavigator navigator, IJokeService jokeService)
		{
			Navigator = navigator;
			JokeService = jokeService;

			GoToSecondPageCommand = new Command(async() => 
			{ await Navigator.PushAsync<SecondPageVM>(); });
			TellJokeCommand = new Command(async () => 
			{ JokeModel = await JokeService.GetNewJokeAsync(); });
		}

		private readonly INavigator Navigator;

		#region Joke Support

		public string FirstLine
		{
			get
			{
				return JokeModel?.FirstLine;
			}
		}

		public string Punchline
		{
			get
			{
				return JokeModel?.Punchline;
			}
		}
		public ICommand TellJokeCommand { get; private set; }

		private readonly IJokeService JokeService;
		private Joke _jokeModel = null;
		private Joke JokeModel
		{
            get { return _jokeModel; }
			set
			{
				if (SetProperty(ref _jokeModel, value))
				{
					OnPropertyChanged(nameof(FirstLine));
					OnPropertyChanged(nameof(Punchline));
				}
			}
		}

		#endregion
	}
}
