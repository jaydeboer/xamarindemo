﻿using DK.XamarinForms.Base.ViewModels;

namespace EveryoneInThePool.ViewModels
{
	public class SecondPageVM : ViewModelBase
	{
		public string Message { get; } = "This is the second view.";
	}
}
