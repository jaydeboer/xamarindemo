﻿using Autofac;
using DK.XamarinForms.Base.Bootstrapping;
using DK.XamarinForms.Base.Factories;
using EveryoneInThePool.ViewModels;
using EveryoneInThePool.Views;
using Xamarin.Forms;

namespace EveryoneInThePool
{
	class Bootstrapper : AutofacBootstrapper
	{
		public Bootstrapper(Application app)
		{
			App = app;
		}

		protected override void ConfigureContainer(ContainerBuilder builder)
		{
			base.ConfigureContainer(builder);

			var assembly = System.Reflection.Assembly.Load(new System.Reflection.AssemblyName("EveryoneInThePool"));
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".Services.Implementation")).AsImplementedInterfaces().SingleInstance();
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".ViewModels")).AsSelf().InstancePerDependency();
			builder.RegisterAssemblyTypes(assembly).Where(t => t.Namespace != null && t.Namespace.EndsWith(".Views")).AsSelf().InstancePerDependency();

			builder.RegisterType<Jokester.Api.ClientAccess.Implementation.JokeRepository>().AsImplementedInterfaces().SingleInstance();
		}

		protected override void ConfigureApplication(IContainer container)
		{
			var viewFactory = container.Resolve<IViewFactory>();
			var mainPage = viewFactory.Resolve<MainPageVM>();
			var navigationPage = new NavigationPage(mainPage);

			App.MainPage = navigationPage;
		}

		protected override void RegisterViews(IViewFactory viewFactory)
		{
			viewFactory.Register<MainPageVM, MainPage>();
			viewFactory.Register<SecondPageVM, SecondPage>();
		}

		private readonly Application App;
	}
}
