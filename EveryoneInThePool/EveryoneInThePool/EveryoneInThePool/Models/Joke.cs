﻿namespace EveryoneInThePool.Models
{
	public class Joke
	{
		public string FirstLine { get; set; }
		public string Punchline { get; set; }

		public static Joke Create(string firstLine, string punchline)
		{
			return new Joke() { FirstLine = firstLine, Punchline = punchline };
		}

	}
}
