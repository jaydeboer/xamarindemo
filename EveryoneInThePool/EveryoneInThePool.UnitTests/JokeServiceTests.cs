﻿using EveryoneInThePool.Services.Implementation;
using Jokester.Api.ClientAccess;
using Jokester.Api.ClientAccess.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace EveryoneInThePool.UnitTests
{
	public class JokeServiceTests : AutomockingBase
	{
		[Test]
		public async Task WhenGetNewJokeIsSuccessful_TheReturnedJokeIsMappedToModel()
		{
			var expectedFirstLine = "Something";
			var expectedPunchline = "funny!";
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ReturnsAsync(
				new JokeApiModel()
				{
					Setup = expectedFirstLine,
					Punchline = expectedPunchline
				});

			var service = mock.Create<JokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(actual.FirstLine, Is.EqualTo(expectedFirstLine));
			Assert.That(actual.Punchline, Is.EqualTo(expectedPunchline));
		}

		[Test]
		public async Task WhenGetNewJokeReceivesNull_ADefaultJokeIsReturned()
		{
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ReturnsAsync(null);

			var service = mock.Create<JokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(string.IsNullOrWhiteSpace(actual.FirstLine), Is.False);
			Assert.That(string.IsNullOrWhiteSpace(actual.Punchline), Is.False);
		}

		[Test]
		public async Task WhenRepositoryThrowsException_ADefaultJokeIsReturned()
		{
			mock.Mock<IJokeRepository>().Setup(
				r => r.GetAsync()).ThrowsAsync(new NullReferenceException());

			var service = mock.Create<JokeService>();

			var actual = await service.GetNewJokeAsync();

			Assert.That(string.IsNullOrWhiteSpace(actual.FirstLine), Is.False);
			Assert.That(string.IsNullOrWhiteSpace(actual.Punchline), Is.False);
		}
	}
}
