﻿using Autofac.Extras.Moq;
using NUnit.Framework;

namespace EveryoneInThePool.UnitTests
{
	[TestFixture]
	public  abstract class AutomockingBase
	{

		AutoMock _mock = null;

		protected AutoMock mock
		{
			get
			{
				if (_mock == null)
				{
					_mock = AutoMock.GetLoose();
				}
				return _mock;
			}
		}

		[TearDown]
		protected virtual void TearDown()
		{
			if (_mock != null)
			{
				_mock.Dispose();
				_mock = null;
			}
		}	
	}
}
