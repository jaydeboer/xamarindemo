﻿using DK.XamarinForms.Base.Services;
using EveryoneInThePool.Services;
using EveryoneInThePool.UnitTests.Mocks;
using EveryoneInThePool.ViewModels;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace EveryoneInThePool.UnitTests
{
	internal class MainPageVMTests : AutomockingBase
	{
		[Test]
		public async Task SecondPageButton_NavigatesToSecondPage()
		{
			var navigator = new MockNavigator();
			mock.Provide<INavigator>(navigator);
			var vm = mock.Create<MainPageVM>();

			vm.GoToSecondPageCommand.Execute(null);
			await Task.Delay(2);

			Assert.That(navigator.CurrentViewModel, Is.InstanceOf<SecondPageVM>());
		}


		[Test]
		public async Task TellJokeCommand_UpdatesFirstLineAndPunchline()
		{
			var expectedFirstLine = "This is the first line";
			var expectedPunchline = "Yup, funny!";
			mock.Mock<IJokeService>().Setup(
				s => s.GetNewJokeAsync()).
				ReturnsAsync(new Models.Joke()
				{
					FirstLine = expectedFirstLine,
					Punchline = expectedPunchline
				});
			var vm = mock.Create<MainPageVM>();

			vm.TellJokeCommand.Execute(null);
			await Task.Delay(2);

			Assert.That(vm.FirstLine, Is.EqualTo(expectedFirstLine));
			Assert.That(vm.Punchline, Is.EqualTo(expectedPunchline));
		}

	}
}
