﻿using Jokester.Api.ClientAccess.Models;
using System.Threading.Tasks;

namespace Jokester.Api.ClientAccess
{
	public interface IJokeRepository
	{
		Task<JokeApiModel> GetAsync();
	}
}
