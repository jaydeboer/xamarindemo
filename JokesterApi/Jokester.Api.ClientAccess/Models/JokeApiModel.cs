﻿namespace Jokester.Api.ClientAccess.Models
{
	public class JokeApiModel
	{
		public string Setup { get; set; }
		public string Punchline { get; set; }
	}
}
