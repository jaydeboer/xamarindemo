﻿using Jokester.Api.ClientAccess.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Jokester.Api.ClientAccess.Implementation
{
	public class JokeRepository : IJokeRepository
	{
		private HttpClient Client = new HttpClient();

		public async Task<JokeApiModel> GetAsync()
		{
			using (var response = await Client.GetAsync("api/joke").ConfigureAwait(false))
			{
				return JsonConvert.DeserializeObject<JokeApiModel>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
			}
		}

		public JokeRepository()
		{
			Client.BaseAddress = new Uri("http://jokester.azurewebsites.net");
		}
	}
}
