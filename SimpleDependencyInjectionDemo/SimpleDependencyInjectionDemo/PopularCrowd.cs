﻿using SimpleDependencyInjectionDemo.Services;

namespace SimpleDependencyInjectionDemo
{
	class PopularCrowd
	{
		public PopularCrowd(ICoolService service)
		{ Service = service; }
		private readonly ICoolService Service;

		public void Promote()
		{ Service.MakeMeCool(); }
	}
}
