﻿using Autofac;
using SimpleDependencyInjectionDemo.Services.Implementation;
using System;

namespace SimpleDependencyInjectionDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<CoolerService>().AsImplementedInterfaces();
			builder.RegisterType<GeekCrowd>().AsSelf();
			builder.RegisterType<CoolService>().AsImplementedInterfaces();
			builder.RegisterType<PopularCrowd>().AsSelf();
			var container = builder.Build();
			var pc = container.Resolve<PopularCrowd>();
			pc.Promote();

			var gc = container.Resolve<GeekCrowd>();
			gc.Promote();

			Console.ReadKey();
		}
	}
}
