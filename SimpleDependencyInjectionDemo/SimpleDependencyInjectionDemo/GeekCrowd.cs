﻿
using SimpleDependencyInjectionDemo.Services;

namespace SimpleDependencyInjectionDemo
{
	class GeekCrowd
	{

		public GeekCrowd(ICoolerService service)
		{
			Service = service;
		}

		private readonly ICoolerService Service;

		public void Promote()
		{
			Service.MakeMeSuperCool();
		}
	}
}
