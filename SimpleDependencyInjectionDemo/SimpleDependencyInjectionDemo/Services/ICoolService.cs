﻿namespace SimpleDependencyInjectionDemo.Services
{
	interface ICoolService
	{
		void MakeMeCool();
	}
}
