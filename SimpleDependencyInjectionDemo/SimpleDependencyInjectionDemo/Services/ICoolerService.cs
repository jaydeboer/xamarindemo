﻿namespace SimpleDependencyInjectionDemo.Services
{
	interface ICoolerService
	{
		void MakeMeSuperCool();
	}
}
