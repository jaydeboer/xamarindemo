﻿using System;

namespace SimpleDependencyInjectionDemo.Services.Implementation
{
	class CoolerService : ICoolerService
	{
		public void MakeMeSuperCool()
		{
			Console.WriteLine("Now you are SUPER COOL!");
		}
	}
}
