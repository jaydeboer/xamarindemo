﻿using System;

namespace SimpleDependencyInjectionDemo.Services.Implementation
{
	class CoolService : ICoolService
	{
		public void MakeMeCool()
		{
			Console.WriteLine("You are now cool.");
		}
	}
}
